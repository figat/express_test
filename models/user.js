const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const User = Schema({
  username: { type: String, unique: true, require: true },
  useremail: { type: String, unique: true, require: true },
  password: { type: String, require: true }
});

console.log(this.password);

module.exports = mongoose.model("UserModel", User);
