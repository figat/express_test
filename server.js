const express = require("express");
const { check, validationResult } = require("express-validator");
const path = require("path");
const session = require("express-session");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const cookieParser = require("cookie-parser");
const flash = require("connect-flash");
const User = require("./models/user");
server = express();
const port = 8080;
const static = path.join(__dirname, "public");
const views = path.join(__dirname, "views");
server.set("view engine", "ejs");
server.use(express.static(static));
server.set("views", views);

server.use(
  bodyParser.urlencoded({
    extended: false
  })
);

server.use(bodyParser.json());
server.use(flash());

server.use(cookieParser());
server.use(
  session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: true
  })
);
server.use((req, res, next) => {
  res.locals.errors = req.flash("errors");
  res.locals.success = req.flash("success");
  next();
});
server.use(morgan("tiny"));
server.use(cors());

const db = process.env.DB_PASSWOR || "mongodb://localhost:27017/express_Bartek";

mongoose
  .connect(db, { useCreateIndex: true, useNewUrlParser: true })
  .then(() => {
    console.log("mongoDB connected");
  })
  .catch(err => {
    console.error(
      `message: ${err.message}  
       codeN:${err.codeName} 
       codeNumber:${err.code} 
       errName:${err.name}
      `
    );
  });

const fName = [
  "Lillie",
  "Samara",
  "Belle",
  "Alma",
  "Norm",
  "Lina",
  "Maisy",
  "Jacinth",
  "Jeff",
  "Jefferson",
  "Eloise",
  "Patti"
];

const fNames = fName.map(items => {
  return items;
});

//@Middleware start
//if not logged in
const notLoggedIn = (req, res, next) => {
  if (!req.session.userId) {
    res.redirect("/login");
  } else {
    next();
  }
};
//if  logged in
const isLoggedIn = (req, res, next) => {
  if (req.session && req.session.userId) {
    res.redirect("/");
  } else {
    next();
  }
};

//@Middleware end

//@get Root Route
//@get Index About Contact Routes
server.get("/", (req, res, next) => {
  res.render("home", { home: "home page", user: req.session.userId });
});

//@get more_info
server.get("/more_info", (req, res, next) => {
  const msg = req.query.msg;
  res.json({ msg });
});

//@get random_names
server.get("/random_names", (req, res, next) => {
  res.json({ fNames });
});

//@get index
server.get("/index", notLoggedIn, (req, res, next) => {
  res.render("index", { index: "index page", user: req.session.userId });
});

//@get about
server.get("/about", (req, res, next) => {
  res.render("about", { about: "about page", user: req.session.userId });
});

//@get contact
server.get("/contact", (req, res, next) => {
  res.render("contact", { contact: "contact page", user: req.session.userId });
});

//@get login
server.get("/login", isLoggedIn, (req, res, next) => {
  res.render("login", { user: req.session.user });
});

//@get register
server.get("/register", isLoggedIn, (req, res, next) => {
  res.render("register", { user: req.session.userId });
});

//login Validation
const userLoginValidation = [
  check("useremail", "This field is required")
    .isEmail()
    .withMessage(),
  check("password", "This field is required").isLength({ min: 6 })
];

//@post login
server.post("/login", isLoggedIn, userLoginValidation, (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = errors.array().map(err => {
      return err.msg;
    });
    req.flash("errors", `${error[0]}`);
    req.flash("errors", `${error[1]}`);
    return res.redirect("/login");
  }
  const { useremail, password } = req.body;

  User.findOne({ useremail })
    .then(user => {
      if (!user) {
        req.flash("errors", `Something went wrong`);
        res.redirect("/login");
        console.log("no user found", user);
      } else {
        console.log(user[0].password);
        bcrypt.compare(password, user[0].password, (err, result) => {
          if (err) {
            console.log("Auth failed_1");
            res.redirect("/login");
          }

          if (result) {
            req.session.userId = user[0].id;
            req.session.userName = user[0].username;
            req.flash("success", `Welcome ${req.session.userName}`);
            return res.redirect("/");
          }

          req.flash("errors", `Something went wrong`);
          res.redirect("/login");
          console.log(
            "=====Auth failed_2===the password does not match===",
            result
          );
        });
      }
    })
    .catch(err => {
      console.log("===Error====", err);
      // res.redirect("/login");
    });
});

//register Validation
const userRegisterValidation = [
  check("username", "Name is require")
    .not()
    .isEmpty(),
  check("useremail", "Plase include valid email").isEmail(),
  check(
    "password",
    "Plase enter a password with 6 or more characters"
  ).isLength({ min: 6 })
];

//@post register
server.post(
  "/register",
  isLoggedIn,
  userRegisterValidation,
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = errors.array().map(err => {
        return err.msg;
      });
      req.flash("errors", `${error[0]}`);
      req.flash("errors", `${error[1]}`);
      req.flash("errors", `${error[2]}`);
      return res.redirect("/register");
    }

    const { username, useremail, password } = req.body;

    User.findOne({ useremail }).then(user => {
      if (user || user === "index: username_1 dup key") {
        req.flash("errors", `Email already exist`);
        res.redirect("register");
        console.log(user, `Email already exist`);
      } else {
        const saltRounds = 10;
        bcrypt.genSalt(saltRounds, (err, salt) => {
          if (err) {
            console.log(err);
          } else {
            bcrypt.hash(password, salt, (err, hash) => {
              if (err) {
                console.log(err);
              } else {
                // Store hash in your password DB.
                const users = new User({ username, useremail, password: hash });
                User.create(users)
                  .then(user => {
                    req.session.user = user;
                    res.redirect("/login");
                  })
                  .catch(err => {
                    req.flash("errors", `Something went wrong`);
                    res.redirect("/register");
                    console.log(err);
                    console.log(err, "err!@#$%^&");
                  });
              }
            });
          }
        });
      }
    });
  }
);

server.get("/logout", function(req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(err => {
      if (err) {
        return res.redirect("/");
      } else {
        res.redirect("/login");
      }
    });
  }
});

const liveReload = require("livereload").createServer({
  exts: ["js", "css", "ejs"]
});

liveReload.watch(views);
liveReload.watch(static);

server.listen(port, () => console.log(`Server works on port:${port}`));
